@extends('layouts.app')
@section('content')


<div class="container sectionmargin">
  <div class="row py-5">
    <div class="col-12 col-md-8"> {{-- listing --}}
      {{-- start carousel --}}
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          @foreach($listing->images as $image)
          <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}" class="active"></li>
          @endforeach
        </ol>
        <div class="carousel-inner">
          @foreach($listing->images as $image)
          <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
            <img class="d-block w-100" src="{{ $image->getUrl(600, 338) }}" alt="{{$listing->title}}">
          </div>
          @endforeach
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
      {{-- end carousel --}}
    </div>
    <div class="col-12 col-md-4 text-center"> {{-- user --}}
      <strong>{{ $listing->category->name }}</strong>
      <p>Inserito il:  {{$listing->created_at->format('d/m/Y')}}</p>
      <hr class="hrstyle">
      <h3 class="" data-aos="fade-up"> {{$listing->title}}</h3>
      <hr class="hrstyle">
      <p class="giallo" style="font-size: 2rem">€ 
        <strong >{{$listing->price}}</strong>
      </p>
      <hr class="hrstyle">
      <div>
        <h4>{{$listing->user->name}}</h4>
        <h6 class="mb-2">{{$listing->user->listings->count()}} annunci inseriti</h6>
        <img src="https://picsum.photos/100/100" alt="Avatar" class="rounded-circle">
      </div>      
    </div>
  </div> {{-- end row --}}
  <div class="row">
    <div class="col-12 col-md-8">
      <h4>Descrizione</h4>
      <p class="mt-3">
        {{$listing->description}}
      </p>
    </div>
    <div class="col-12 col-md-4 text-center my-5">
      <button type="button" class="site-btn sb-big rounded" data-toggle="modal" data-target="#exampleModal">
        Contatta!
      </button>
      <!-- Modal -->
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Contatta il venditore</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group text-black">
                <input name="name" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="richiestaHelp" placeholder="Nome">
              </div>
              <div class="form-group">
                <textarea name="message" rows="4" class="form-control" placeholder="{{__('ui.message')}}"></textarea>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-warning" href="mailto:{{$listing->user->email}}">Invia</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>  {{-- container --}}
@endsection
