@extends('layouts.app')
@section('content')    
    <section class="sectionmargin">
        <div class="container pt-4">
            <div class="shadow-lg p-3 mb-5">
                
                <ul class="nav nav-tabs search-type">
                    <li class="st-item nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#reg" role="tab"><label class="m-0" for="reg"><i class="fas fa-infinity giallo margin-right: 0px;"></i><br><span class="giallo">{{__('ui.register')}}</span></label></a> 
                    </li>
                    
                    <li class="st-item nav-item">
                        <a class="nav-link" data-toggle="tab" href="#log" role="tab"><label class="m-0" for="log"><i class="fas fa-infinity giallo"></i><br><span class="giallo">{{__('ui.login')}}</span></label></a> 
                    </li>
                    
                </ul>
            
                {{-- content tab --}}
                <div class="tab-content">
                    
                    {{-- register pane tab --}}
                    <div id="reg" class="card-body tab-pane fade show active" role="tabpanel">
                        <form method="POST" action="{{ route('register') }}">

                            @csrf
                            
                            <div class="form-group row">
                                
                                <div class="col-12">
                                    <input id="name" type="text" type="email" placeholder="{{ __('ui.name') }}" class="form-control customfont @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                    
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                
                            </div>
                            
                            <div class="form-group row">
                                <div class="col-12">
                                    <input id="email" type="email" placeholder="{{ __('ui.mail') }}" class="form-control customfont @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                                    
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <div class="col-12">
                                    <input id="password" type="password" placeholder="{{ __('Password') }}" class="form-control customfont @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                    
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <div class="col-12">
                                    <input id="password-confirm" placeholder="{{__('ui.confirm') }} Password" type="password" class="form-control customfont" name="password_confirmation" required autocomplete="new-password">
                                </div>
                            </div>
                            
                            <div class="form-group row mb-0">
                                <div class="col-md-6 text-center mx-auto">
                                    <button type="submit" class="site-btn sb-big rounded mt-3">
                                        {{ __('ui.register') }}
                                    </button>
                                </div>
                            </div>

                        </form>
                        <a href="{{url('/redirect')}}"><i class="fab fa-google-plus fa-2x" style="color: #E44134"></i></a>
                        <a  href="{{route('login.social',['service'=>'facebook'])}}">
                            <i class="fab fa-facebook fa-2x" style="color: #4267B2"></i>
                         </a>
                    </div> {{-- / end register pane tab --}}                
                    
                    
                    {{-- login pane tab --}}
                    <div id="log" class="card-body tab-pane fade" role="tabpanel">
                        <form method="POST" action="{{ route('login') }}">

                            @csrf
                            
                            <div class="form-group row">
                                
                                <div class="col-12">
                                    <input id="email" type="email" placeholder="{{ __('ui.mail') }}" class="form-control customfont @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                                    
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                
                                <div class="col-12">
                                    <input id="password" type="password" placeholder="{{ __('Password') }}" class="form-control customfont @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                    
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            
                            <div class="form-group row mb-0">
                                <div class="col-12 col-md-6 text-center m-auto">
                                    <button type="submit" class="site-btn sb-big rounded mt-3">
                                        {{ __('ui.login') }}
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div> {{-- / end login pane tab --}} 

                </div> {{-- end tab content --}}    

            </div>
        </div>
    </section>
        
@endsection
        
        
        
        