@extends('layouts.app')
@section('content')

<section class="sectionmargin">
  <div class="container pt-4">  
    <div class="hero-warp shadow-lg p-3 mb-5">
      
      @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif

      <div class="form-search-wrap mb-3" data-aos="fade-up" data-aos-delay="200">
        <!-- DEBUG::  SECRET{{$uniqueSecret}} -->
        <form method="post" action="{{route('createlisting')}}" enctype="multipart/form-data">
          @csrf
          <input type="hidden" name="uniqueSecret" value="{{$uniqueSecret}}">
          <div class="row align-items-center justify-content-center">
            <div class="col-lg-12 mb-4 mb-xl-0 col-xl-10 py-2 text-center">
              <div class="mb-4">
                <h2>Inserisci il tuo annuncio</h2>
              </div>
              <input type="text" name="title" value="{{ old('title') }}" class="form-control customfont rounded" placeholder="{{__('ui.what-sell')}}?">
            </div>
            <div class="col-lg-12 mb-4 mb-xl-0 col-xl-10 py-2">
              <textarea type="text" name="description"  class="form-control customfont rounded" placeholder="{{__('ui.description')}}">{{ old('description') }}</textarea>
            </div>
            <div class="col-lg-12 mb-4 mb-xl-0 col-xl-10 py-2">
              <input type="text" name="price" value="{{ old('price') }}" class="form-control customfont rounded" placeholder="{{__('ui.price')}}">
            </div>
            <div class="col-12 col-lg-8 col-xl-10 py-2">
              <div class="select-wrap">
                <span class="icon"><span class="icon-keyboard_arrow_down"></span></span>
                <select class="form-control customfont rounded" name="category" id="">
                  @foreach($categories as $category)
                  <option value="{{$category->id}}">{{$category->name}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            
            <div class="form-search-wrap mb-3">
              
              <div class="col-12 p-0">
                <div class="dropzone" id="drophere" style="width: 920px">
                  @error('images')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{$message}}</strong>
                  </span>
                  @enderror
                  <div class="dz-message" data-dz-message><span>Carica o trascina le immagini qui!</span></div>

                </div>
              </div>
            </div>   
          </div>  
          <div class="col 12 col-lg-8 col-xl-6 pt-2 text-center m-auto">
            <input type="submit" class="site-btn sb-big rounded" value="{{__('ui.sell')}}">
          </div>
        </form>
      </div> {{-- form search wrap --}} 
    </div> {{-- hero warp --}}
  </div> {{-- container --}}
</section>

@endsection