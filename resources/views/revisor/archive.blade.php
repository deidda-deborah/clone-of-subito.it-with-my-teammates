@extends('layouts.app')
@section('content')
@if($listings) 
<section class="container sectionmargin">
    <section class="row pt-4 justify-content-center">
        <div class="col-12">
            <h4>Admin Panel</h4>
        </div>
    </section>
    
    
    <section class="py-3">
        @foreach ($listings as $listing)
        <div class="col-12 rounded bordogiallo p-2 mb-2">
            <div class="col-12 p-1">
                <strong>{{$listing->title}}</strong>
                <hr>
            </div>

            <div class="col-12 p-2 bordoceleste">
                <div class="row">
                    <div class="col-4">
                        @foreach($listing->images as $image)
                            <img src="{{ $image->getUrl(300, 169) }}" alt="" class="m-1">
                        @endforeach
                    </div>
                    <br>
                </div>
            </div> 
            
            <hr>
            
            <div class="row">
                <div class="col-12 col-md-6">
                    <br>
                    <strong>Data di inserimento:</strong><br>
                    {{$listing->created_at}}
                    <br>
                </div>
            
                <div class="col-12 col-md-6">
                    <br>
                    <strong>Descrizione:</strong><br>
                    {{$listing->description}}                 
                    <br>
                </div>
                <br>
            </div>

            <hr>
            
            <div class="row justify-content-center">
                <div class="col-12 col-md-2 text-center">
                    
                    <form action="{{route('revisor.return', $listing->id)}}" method="post">
                        @csrf
                        <button type="submit" class="btn btn-lg btn-success"><i class="fas fa-undo"></i></button>
                    </form>

                </div>
            </div>

        </div>
        @endforeach
    </section>
    
    
    
    
    @else
    <section class="container sectionmargin">
        <div class="row my-4">
            <div class="col-12 my-4">
                <br>
                <h5>Non ci sono annunci da revisionare.</h5>
                <br>
            </div>
        </div>
    </section>
    @endif
    @endsection
