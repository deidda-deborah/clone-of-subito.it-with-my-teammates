@extends('layouts.app')
@section('content')

@if($listings) 
<section class="container sectionmargin">
    <section class="row pt-4 justify-content-center">
        <div class="col-12">
            <h4>Admin Panel</h4>
        </div>
    </section> {{-- admin panel --}}
    
    <section class="py-3">
        @foreach ($listings as $listing)
        <div class="col-12 rounded bordogiallo p-2 mb-2">
            <div class="col-12 p-1">
                <strong>{{$listing->title}}</strong>
                <hr>
            </div>{{-- title --}}           
            
            <div class="col-12 p-2 bordoceleste"> 
                @foreach($listing->images as $image)
                <div class="row">
                    <div class="col-4">
                        <img src="{{ $image->getUrl(300, 169) }}" alt="{{$listing->title}}" class="m-1">
                    </div> 

                    <div class="col-4">
                        <strong>Flags</strong><br>
                        Adult: <button type="button" class="btn btn-sm btn-outline-dark m-1" value="Adult" disabled><i class="fas fa-venus-mars"></i> {{ $image->adult }}</button><br>
                        Medical: <button type="button" class="btn btn-sm btn-outline-dark m-1" value="medical" disabled><i class="fas fa-syringe"></i> {{ $image->medical }}</button><br>
                        Spoof: <button type="button" class="btn btn-sm btn-outline-dark m-1" value="spoof" disabled><i class="fas fa-mask"></i> {{ $image->spoof }}</button><br>
                        Violence: <button type="button" class="btn btn-sm btn-outline-dark m-1" value="violence" disabled><i class="fas fa-fist-raised"></i> {{ $image->violence }}</button><br>
                        Racy: <button type="button" class="btn btn-sm btn-outline-dark m-1" value="racy" disabled><i class="fas fa-user-alt-slash"></i> {{ $image->racy }}</button><br>
                    </div>
                        
                    <div class="col-4">
                        <strong>Labels</strong><br>
                        <ul>
                            @if($image->labels)
                            @foreach($image->labels as $label)
                            <button type="button" class="btn btn-sm btn-outline-dark m-1" disabled>{{ $label }}</button>           
                            @endforeach
                            @endif                              
                        </ul>
                    </div>   
                    
                </div> {{-- img row --}}
                <hr>
                @endforeach {{-- for each image --}}   
            </div> {{-- img col --}}

            <div class="row">
                <div class="col-6">
                    <br>
                    <strong>Descrizione</strong><br>
                    {{$listing->description}}
                    <br>
                </div>
                <div class="col-6">
                    <br>
                    <strong>Data Creazione</strong><br>
                    {{$listing->created_at}}                 
                    <br>
                </div>
                <br>    
            </div> {{-- row --}}
            <hr>  
            <div class="row justify-content-center">
                <div class="col-2 text-center">
                    <form action="{{route('revisor.reject', $listing->id)}}" method="post">
                        @csrf
                        <button type="submit" class="btn btn-lg btn-danger"><i class="fas fa-times p-1"></i></button>
                    </form>
                </div>
                <div class="col-2 text-center">
                    <form action="{{route('revisor.accept', $listing->id)}}" method="post">
                        @csrf
                        <button type="submit" class="btn btn-lg btn-success"><i class="fas fa-check p-1"></i></button>
                    </form> 
                </div>
                
            </div>

        </div> {{-- col annuncio --}}    
        @endforeach
    </section> {{-- se --}}      

</section> {{-- main section --}}         

@else
<section class="container sectionmargin">
    <div class="row my-4">
        <div class="col-12 my-4">
            <br>
            <h5>Non ci sono annunci da revisionare.</h5>
            <br>
        </div>
    </div>
</section>
@endif
@endsection