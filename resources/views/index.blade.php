@extends('layouts.app')

@section('title', 'Home')
@section('description', 'Questo sito fa gli annuci megagalattici superfighiximi')



@section('content')
<section>
    @if(session('access.denied.revisor.only'))
    <div class="alert alert-danger">
        Accesso negato, solo revisori.
    </div>
    @endif
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
    @extends('layouts._searchform') 
</section>

<section class="search-result-section">
    <div class="container ">
         <div class="row {{--justify-content-center my-auto shadow-lg p-3 mb-5--}}"> 
            <div class="col-12 col-8">
                <div class="bd-example">
                        <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                    @foreach($listings->chunk(3) as $chunk)
                                <li data-target="#carouselExampleCaptions" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
                                @endforeach
                            </ol>
                            <div class="carousel-inner">
                                @foreach ($listings->chunk(3) as $chunk)
                                <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                                    <div class="row">
                                        @foreach ($chunk as $listing)
                                        <div class="col-sm-4">
                                            <div class="view">
                                            <a href="{{route('listing', ['id'=>$listing->id])}}">
                                            <img src="{{ optional($listing->images->first())->getUrl(600, 338) }}" class="d-block w-100  rounded-sm" alt="{{$listing->title}}">
                                        
                                        <div class="mask rgba-black-strong"></div>
                                    </a>
                                            </div>
                                            
                                            <div class="carousel-caption d-none d-md-block p-0 ">
                                                <h5 class="text-white">{{$listing->title}}</h5>
                                                <p class="giallo m-0" style="font-size: 1rem">€ 
                                                    <strong class="">{{$listing->price}}</strong>
                                                </p>
                                            </div>
                                            
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
        </div>
            {{-- @foreach ($listings as $listing)
            <a href="{{route('listing', ['id'=>$listing->id])}}" class="readmore-btn">
                <!--  <div class="col-md-2 mt-2 ">
                    <div class="property-item shadow-lg rounded p-1 "> -->
                        <div class=" col-md-2 mt-2 pi-image property-item shadow-lg rounded p-1 bordoceleste mx-1 ">
                            <a href="{{route('listing', ['id'=>$listing->id])}}">
                                <img src="{{ optional($listing->images->first())->getUrl(150, 150) }}" alt="">
                            </a>
                            <div class="pi-badge new rounded ">Nuovo</div>
                            
                            <h3 class="giallo">€ {{$listing->price}}</h3>
                            <h5 class="mb-1">{{$listing->title}}</h5>
                            {{__('ui.on')}} <strong><a href="{{ route('listings.by.category', ['name' => $listing->category->name, 'id' => $listing->category->id]) }}">{{ $listing->category->name }}</a></strong>
                            
                        </div>
                        <!--   </div>
                        </div> -->
                    </a>
                    @endforeach    --}}
                </div>
                {{-- row --}}
                <div class="row my-3">
                    <div class="col-12 d-flex justify-content-center">
                        <a href="/annunci"><button class="site-btn sb-big rounded">{{__('ui.all-list')}}</button></a>
                    </div>
                </div> {{-- row --}}
            </div>
        </section>
       <!--Carousel Wrapper-->


@endsection