@extends('layouts.app')
@section('content')

<section class="container sectionmargin">
        <div class="row my-4">
            <div class="col-12 my-4">
                <br>
                <h6 class="alert alert-success text-center m-auto" role="alert">{{__('ui.thanks')}}</h6>
                <br>
            </div>
        </div>
    </section>
@endsection