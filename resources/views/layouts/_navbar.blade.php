<div class="container">
    <header class=" header-section">
        <a href="/" class="site-logo">
            <h2 class="text-warning oversized"><i class="fas fa-cart-arrow-down rotate"></i> presto </h2>
        </a>

        <nav class="header-nav">
            <ul class="main-menu">
                    
                <li><a href="/" class="text-warning ">Home</a></li>    
                <li><a href="{{route('listings')}}" class="text-warning">{{ __('ui.alllistings')}}</a></li>
                    
                    
                    
                {{-- register/login tabs --}}
                @guest
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link text-warning" href="{{ route('register') }}">{{ __('ui.reg_log') }}</a>
                        </li>
                        {{-- <a class="btn btn-link" href="{{route('login.social',['service'=>'facebook'])}}">
                            facebook
                        </a>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                              <a href="{{url('/redirect')}}" class="btn btn-primary">Login with Google</a>
                            </div>
                        </div> --}}
                    @endif
                    @else               
                        @if(Auth::user()->is_revisor)
                            <li class="nav-item">
                                <a class="nav-link text-warning" href="{{ route('revisor.home') }}">Revisor home
                                    
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a class="dropdown-item text-warning " href="{{ route('revisor.home') }}">Nuovi annunci
                                        <span class="badge badge-pill badge-warning">{{\App\Listing::ToBeRevisionedCount()}}</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item text-warning " href="{{ route('revisor.archive') }}">Archivio annunci
                                        <span class="badge badge-pill badge-warning">{{\App\Listing::ToBeArchiveCount()}}</span></a></li>
                                </ul>
                            </li>
                        @endif
                    
                    <li>
                        <a href="#" class="text-warning"> {{ Auth::user()->name }}</a>
                        <ul class="sub-menu">
                            <li> 
                                <a class="dropdown-item text-warning" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                                </a>
                            
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </li>
                @endguest
                
                <li>
                    @guest
                        <a href="{{ route('register') }}" class="cta">
                            @else
                                <a href="{{route('newlisting')}}" class="cta">
                    @endguest
                            <span class="text-warning rounded">{{__('ui.publish')}}</span>
                        </a>
                    </a>
                </li>


                <li>
                    <a href="#" class="text-warning"><i class="fas fa-globe"></i></a>
                    <ul class="sub-menu">
                        @include('layouts._locale', ['lang' => 'it','nation' =>'it'])
                        @include('layouts._locale', ['lang' => 'en','nation' =>'gb'])
                        @include('layouts._locale', ['lang' => 'es','nation' =>'es'])
                    </ul>
                </li>
                
                
                
            </ul> {{-- main menu ul --}}
            
        </nav>
        
        <hr class="hrstyle">
    </header>

</div>
