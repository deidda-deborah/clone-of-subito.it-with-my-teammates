

<div class="container mb-3">
	<hr class="hrstyle">
	<footer class="footer-section">
		
		<div class="row justify-content-center">
			
			<div class="col-lg-3 pt-2 text-center">
				<h2 class="text-primary larger"><i class="fas fa-cart-arrow-down rotate"></i> presto</h2>
				<p>{{__('ui.buy-sell-evry')}}</p>

				<a href="" data-toggle="modal" data-target="#contact-modal" class="site-btn sb-big rounded	">
				
				{{__('ui.work-with-us')}}
				</a>	
				
			</div>
			
			{{-- modal --}}
			@guest
			<div  class="modal text-dark" id="contact-modal" tabindex="-1" role="dialog">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title text-danger">E' Necessario essere Loggati per fare domanda!</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						
						<div class="footerlogin">	
							<a class="nav-link text-warning" href="{{ route('register') }}">{{ __('ui.register') }}</a>
									
						</div>
					</div>
				</div>
			</div>
			@else
			<div  class="modal text-dark" id="contact-modal" tabindex="-1" role="dialog">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title text-dark">Contattaci</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							
						</div>
						<div class="modal-body text-black">
							
							<form action="{{route('submit')}}" method="post" >
								@csrf
								<div class="form-group text-black">
									
									<input name="request" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="richiestaHelp" placeholder="{{__('ui.object')}}">
									
								</div>
								<div class="form-group">
									
									<textarea name="message" rows="4" class="form-control" placeholder="{{__('ui.message')}}"></textarea>
								</div>
								<small id="richiestaHelp" class="form-text text-muted">{{__('ui.really-sure')}}?</small>
								<div class="form-group form-check">
									<input name="revisor" type="checkbox" class="form-check-input" id="exampleCheck1">
									<label class="form-check-label" for="exampleCheck1">{{__('ui.so-sure')}}</label>
								</div>
								<button type="submit" class="site-btn  rounded">{{__('ui.send-request')}}</button>
							</form>
						</div>
						<div class="modal-footer">
							
						</div>
					</div>
				</div>
				
			</div>
			@endguest
			
			<div class="col-lg-3 pt-2 text-center">
				<h4>Presto</h4>
				<ul>
					<li><a href="#">{{__('ui.help')}}</a></li>
					<li><a href="#">{{__('ui.rules')}}</a></li>
					<li><a href="#">Privacy</a></li>
				</ul>
				
			</div>
			
			
			<div class=" col-lg-3 pt-2 text-center">
				<h4><i class="fas fa-map-marked-alt fa-lg ml-3 mb-3"></i></h4>
				<ul>
					<li><a href="#"><i class="fas fa-map-marker-alt mr-1 mb-2" style="color:#F1543F"></i>Cagliari</a></li>
					<li><a href="#"><i class="fas fa-map-marker-alt mr-1" style="color:#F1543F"></i>{{__('ui.london')}}</a></li>
				</ul>
			</div>
			<div class="col-lg-3 pt-2 text-center">
				
				<h4>{{__('ui.follow')}}</h4>
				
					<a href="#"><i class="fab fa-facebook-square fa-2x mr-2 mt-3" style="color:blue"></i></a>
					<a href="#"><i class="fab fa-twitter-square fa-2x mr-2" style="color: #1da1f2"></i></a>
					<a href="#"><i class="fab fa-reddit-square fa-2x" style="color:rgb(255, 69, 0)"></i></a>
				
			</div>
		</div> {{-- row --}}
	</footer>
</div> {{-- container --}}