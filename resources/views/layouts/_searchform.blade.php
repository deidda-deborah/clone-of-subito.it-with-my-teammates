<section class="sectionmargin" {{-- class="hero-section set-bg bglur" data-setbg="https://miro.medium.com/max/1018/1*iAu65xDmvpVdBJgps6EDEw.png" --}}>
    <div class="container pt-4">
        <div class=" shadow-lg p-3 mb-5">
            <form class="main-search-form" action="{{route('search')}}" method="GET">
                <div class="search-type">
                    <div class="st-item">
                        <input type="radio" name="cat" value="0" id="tutte">
                        <label for="tutte"><i class="fas fa-infinity giallo"></i><br><span class="giallo">{{__('ui.all')}}</span></label>
                    </div>
                    
                    <div class="st-item">
                        <input type="radio" name="cat" value=1 id="abbigliamento">
                        <label for="abbigliamento"> <i class="fas fa-tshirt giallo"></i><br><span class="giallo">{{ __('ui.clothing')}}</span></label>
                        
                    </div>
                    <div class="st-item celeste">
                        <input type="radio" name="cat" value=2 id="antiquariato">
                        <label for="antiquariato"> <i class="fas fa-scroll giallo"></i><br><span class="giallo">{{__('ui.antique')}}</span></label>
                    </div>
                    <div class="st-item">
                        <input type="radio" name="cat" value=3 id="auto">
                        <label for="auto"> <i class="fas fa-car giallo"></i><br><span class="giallo">{{__('ui.car')}}</span></label>
                    </div>
                    <div class="st-item">
                        <input type="radio" name="cat" value=4 id="case">
                        <label for="case"> <i class="fas fa-home giallo"></i><br><span class="giallo">{{__('ui.house')}}</span></label>
                    </div> 
                    <div class="st-item">
                        <input type="radio" name="cat" value=5 id="elettronica">
                        <label for="elettronica"> <i class="fas fa-laptop giallo"></i><br><span class="giallo">{{__('ui.electronic')}}</span></label>
                    </div>                                          
                    <div class="st-item">
                        <input type="radio" name="cat" value=6 id="giochi">
                        <label for="giochi"> <i class="fas fa-dice giallo"></i><br><span class="giallo">{{__('ui.games')}}</span></label>
                    </div>         
                    <div class="st-item">
                        <input type="radio" name="cat" value=7 id="gioielleria">
                        <label for="gioielleria"> <i class="fas fa-gem giallo"></i><br><span class="giallo">{{__('ui.jewelry')}}</span></label>
                    </div>          
                    <div class="st-item">
                        <input type="radio" name="cat" value=8 id="miscellanea">
                        <label for="miscellanea"> <i class="fas fa-plus-circle giallo"></i><br><span class="giallo">{{__('ui.miscellany')}}</span></label>
                    </div> 
                    <div class="st-item">
                        <input type="radio" name="cat" value=9 id="motori">
                        <label for="motori"> <i class="fas fa-motorcycle giallo"></i><br><span class="giallo">{{__('ui.motor')}}</span></label>
                    </div> 
                    <div class="st-item">
                        <input type="radio" name="cat" value=10 id="sport">
                        <label for="sport"> <i class="fas fa-volleyball-ball giallo"></i><br><span class="giallo">{{__('ui.sport')}}</span></label>
                    </div> 
                </div>
                <div class="search-input rounded text-center">
                    <input type="text" name="q" placeholder="{{__('ui.find')}} {{__('ui.something')}} {{__('ui.di')}} {{__('ui.amazing')}}" class=" rounded text-center">
                    <button type="submit" class="site-btn rounded ">{{__('ui.search')}}</button>
                </div>
                
            </form>
        </div>

</section>