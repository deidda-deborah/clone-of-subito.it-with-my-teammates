<li class="nav-item">
    <form action="{{ route('locale', $lang)}}" method="POST">
        @csrf
        <button type="submit" class="nav-link bglang">
            
            <span class="flag-icon flag-icon-{{$nation}}"> </span>
            
        </button>
    </form>
</li>