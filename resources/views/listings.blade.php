@extends('layouts.app')
@section('content')

<section class="container justify-content-center sectionmargin pt-5 " >
    @foreach ($listings as $listing)
    <section class="row  mb-4 bordoceleste rounded">
        <div class="col-12 col-lg-4 p-0 ">
            <a href="{{route('listing', ['id'=>$listing->id])}}">
                <img class="rounded-sm" src="{{ optional($listing->images->first())->getUrl(300, 169) }}" alt="{{$listing->title}}">
            </a>
        </div>
        <div class="col-12 col-lg-8 b">
            <a href="{{route('listing', ['id'=>$listing->id])}}" style="color: black">
                <span class="" style=" font-size: 2rem;"><strong>{{$listing->title}}</strong></span>
            </a>
            <hr style="margin: 2px 0px 0px 0px">
            <p class="giallo m-0" style="font-size: 1.5rem">€ 
                <strong >{{$listing->price}}</strong>
            </p>
            <p class="m-0">
                {{__('ui.created-at')}} : <strong>{{$listing->created_at->format('d/m/Y')}}
                    in <a href="{{ route('listings.by.category', ['name' => $listing->category->name, 'id' => $listing->category->id]) }}">{{ $listing->category->name }}</a></strong>
                </p>
            </div>
        </section>
        @endforeach
        {{ $listings->links() }}
    </section>
    @endsection
