<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListingImagesTable extends Migration
{
    
    public function up()
    {
        Schema::create('listing_images', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('file');

            $table->unsignedBigInteger('listing_id');
            $table->foreign('listing_id')->references('id')->on('listings');

            $table->timestamps();
        });
    }

    
    public function down()
    {
        Schema::dropIfExists('listing_images');
    }
}
