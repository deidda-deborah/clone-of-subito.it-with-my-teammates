<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->string('name');
            //1- Abbigliamento, 2- Antiquariato, 3- Auto, 4- Case, 5- Elettronica, 6- Giochi, 7- Gioielleria, 8- Miscellanea, 9- Motori, 10- Sport.
                            
            $table->timestamps();
        });
    }

    
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
