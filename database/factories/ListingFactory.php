<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Model;
use App\Listing;
use App\Category;
use Faker\Generator as Faker;

$factory->define(Listing::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence($nbWords = 4, $variableNbWords = true),
        'img'=> $faker->imageUrl($width = 640, $height = 480),
        'description' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'price' =>  $faker->numberBetween($min = 1, $max = 9999999),
        'category_id' =>  $faker->randomElement(Category::pluck('id')->toArray()),
        'user_id' => $faker->randomElement(User::pluck('id')->toArray()),
    ];
});
