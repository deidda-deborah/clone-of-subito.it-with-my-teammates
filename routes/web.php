<?php





Auth::routes();

Route::get('/', 'FrontController@index')->name('index');

Route::get('/annuncio/newlisting', 'FrontController@newlisting')->name('newlisting');
Route::get('/risultati', 'ListingsController@search')->name('search');


Route::post('/annuncio/create', 'FrontController@createlisting')->name('createlisting');
Route::get('/annuncio/thankyou', 'FrontController@thankyou')->name('thankyou');
Route::post('/annuncio/images/upload', 'FrontController@uploadImage')->name('listing.images.upload');
Route::delete('/listing/images/remove', 'FrontController@removeImage')->name('listing.images.remove');
Route::get('/listing/images', 'FrontController@getImages')->name('listing.images');



Route::get('/annunci', 'ListingsController@listings')->name('listings');

Route::get('/annuncio/{id}', 'ListingsController@listing')->name('listing');
Route::get('/category/{name}/{id}/listings', 'ListingsController@listingsByCategory')->name('listings.by.category');

//revisor area
Route::get('/revisor/home', 'RevisorController@homeRevisor')->name('revisor.home');
Route::post('/revisor/listings/{id}/accept', 'RevisorController@accept')->name('revisor.accept');
Route::post('revisor/listings/{id}/reject', 'RevisorController@reject')->name('revisor.reject');
Route::get('/revisor/archive', 'RevisorController@archive')->name('revisor.archive');
Route::post('/revisor/listings/{id}/return', 'RevisorController@return')->name('revisor.return');
Route::delete('/listings/{id}/delete', 'RevisorController@delete')->name('listing.delete'); //da fare!!!!!

Route::post('/contacts/submit', 'ContactsController@submit')->name('submit');

Route::post('/locale/{locale}', 'FrontController@locale')->name('locale');

//view per visualizzare cinque annunci in homepage
Route::get('/', 'ListingsController@last_listings')->name('index');

//rotte per login facebook
Route::get ( '/redirect/{service}', 'SocialAuthController@redirect' )->name('login.social');
Route::get ( '/callback/{service}', 'SocialAuthController@callback' );
//rotte per login google
Route::get('/redirect', 'SocialAuthGoogleController@redirect');
Route::get('/callback', 'SocialAuthGoogleController@callback');
