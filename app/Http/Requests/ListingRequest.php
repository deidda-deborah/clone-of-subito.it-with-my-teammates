<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ListingRequest extends FormRequest
{
    
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
    
            'title'=>'required|max:160',
            'description'=>'required|max:10000',
            'price'=>'required|regex:/^\d+(\.\d{1,2})?$/',
            // 'img'=>'required|image',
            // 'category_id'=>'required'
        ];
    }
}
