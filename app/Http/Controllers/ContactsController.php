<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ContactReceived;
use Illuminate\Support\Facades\Mail;

class ContactsController extends Controller
{
    public function submit(Request $request){

        $req = $request->input('request');
        $message = $request->input('message');
        $get_revisor = $request->input('revisor');

        $adminMail = 'deborah.deidda@deborah.it';
        $bag = compact('req', 'message', 'get_revisor');
        $contactMail = new ContactReceived($bag);
        Mail::to($adminMail)->send($contactMail);
        
        return redirect('/')->with('status', 'La tua richiesta è stata inoltrata.');
    }


}
