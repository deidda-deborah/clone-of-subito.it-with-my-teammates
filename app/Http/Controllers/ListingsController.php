<?php

namespace App\Http\Controllers;

use \App\Listing;
use App\Category;
use Illuminate\Http\Request;

class ListingsController extends Controller
{
   public function listings()
    {
        
        $listings=Listing::orderBy('created_at', 'desc')
        ->where('is_accepted', true)
        ->paginate(10);
        

        return view('listings', compact ('listings'));

    }
    
    public function listing($id)
    {
        $listing = Listing::find($id);
       
        $listings = Listing::where('is_accepted', true)
        ->where('category_id', $listing->category_id)
        ->orderBy('created_at', 'desc')
        ->take(5)
        ->get();
        
        return view('listing', compact('listing', 'listings'));
    }

    public function last_listings()
    {
        $listings=Listing::where('is_accepted', true)
        ->orderBy('created_at', 'desc')
        ->take(5)
        ->get();

        return view('index', compact ('listings'));

    }

    public function listingsByCategory($name, $category_id)
    {
        $category = Category::find($category_id);
        $listings = $category
        ->listings()
        ->where('is_accepted', true)
        ->orderBy('created_at', 'desc')
        ->paginate(10);

        return view('categoria_annunci', compact('category', 'listings'));

    }
    

    public function search(Request $request)
    {
        $q= $request->input('q');
        $cat= $request->input('cat');
        
       
        if($cat==null||$cat==0){
            $listings = Listing::search($q)->get();
            } else{
                if($q==null){
                    $listings = Listing::search($cat)->where('category_id',$cat)->get();
                }else{
            $listings = Listing::search($q)->where('category_id',$cat)->get();
            }}
        //Ricerca con controllo sul database se un annuncio è visibile o meno
        //Implementare questa funzione una volta fatte le validations per l'admin 
        /* $listings = Listing::search($q)->where('visible', true)->get(); */
        return view('search', compact ('q','listings'));

    }

    
}
