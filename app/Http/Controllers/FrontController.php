<?php

namespace App\Http\Controllers;

use App\Listing;
use App\Category;
use App\ListingImage;
use App\Jobs\ResizeImage;
use App\Jobs\AddWaterMark;
use Illuminate\Http\Request;
use App\Jobs\GoogleVisionLabelImage;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\File;
use App\Http\Requests\ListingRequest;
use App\Jobs\GoogleVisionRemoveFaces;
use Illuminate\Support\Facades\Storage;
use App\Jobs\GoogleVisionSafeSearchImage;

class FrontController extends Controller
{
    public function index(){
        
        return view('index');
    }

    public function newlisting(Request $request){
        
        $uniqueSecret = $request->old(
            'uniqueSecret', 
            base_convert(sha1(uniqid(mt_rand())), 16,36));

        return view('newlisting', compact('uniqueSecret'));

    }

    public function createlisting(ListingRequest $request){
        
        $listing = new Listing();

        $listing->title = $request->input('title');
        $listing->description = $request->input('description');
        $listing->price = $request->input('price');
        // $listing->img = $request->input('img');
        $listing->category_id = $request->input('category');
        $listing->user_id = Auth::user()->id;

        $listing->save();

        $uniqueSecret = $request->input('uniqueSecret');
    
        $images = session()->get("images.{$uniqueSecret}", []);
        
        $removedImages = session()->get('removedimages.{$uniqueSecret}', []);

        $images = array_diff($images, $removedImages);

        foreach($images as $image){

            $i = new ListingImage();

            $fileName = basename($image);
            $newFileName = "public/listings/{$listing->id}/{$fileName}";

            Storage::move($image, $newFileName);

            

            $i->file = $newFileName;
            $i->listing_id = $listing->id;

            $i->save();


            GoogleVisionSafeSearchImage::withChain([
                    new GoogleVisionLabelImage($i->id),
                    new GoogleVisionRemoveFaces($i->id),

                    new AddWaterMark($i->getUrl()),

                    new ResizeImage(
                        $i->file,
                        300, 
                        169
                    ),
                    new AddWaterMark($i->getUrl(300,169)),
        
                    new ResizeImage(
                        $i->file, 
                        150, 
                        150
                    ),
                    new AddWaterMark($i->getUrl(150,150)),

        
                    new ResizeImage(
                        $i->file, 
                        600, 
                        338
                    ),
                    new AddWaterMark($i->getUrl(600,338)),


            ])->dispatch($i->id);

            
        }

        File::deleteDirectory(storage_path("app/public/temp/{$uniqueSecret}"));
        
        return redirect(route('thankyou'));
    }

    public function thankyou(){
        
        return view('thankyou');

    }

    public function uploadImage(Request $request){
        
        $uniqueSecret = $request->input('uniqueSecret');
        
        $fileName = $request->file('file')->store("public/temp/{$uniqueSecret}");

        dispatch(new ResizeImage(
            $fileName, 
            120, 
            120
        ));
        
        session()->push("images.{$uniqueSecret}", $fileName);

        return response()->json(
            [
                'id' => $fileName
            ]
        );

    }

    public function removeImage(Request $request){

        $uniqueSecret = $request->input('uniqueSecret');

        $fileName = $request->input('id');

        session()->push("removedimages.{$uniqueSecret}", $fileName);

        Storage::delete($fileName);

        return response()->json('ok');

    }

    public function getImages(Request $request){

        $uniqueSecret = $request->input('uniqueSecret');

        $images = session()->get("images.{$uniqueSecret}", []);
        
        $removedImages = session()->get('removedimages.{$uniqueSecret}', []);

        $images = array_diff($images, $removedImages);

        $data = [];

        foreach($images as $image){

            $data[] = [

                'id' => $image,
                'src' => ListingImage::getUrlByFilePath($image, 120, 120)
            ];

        }

        return response()->json($data);

    }

    public function locale($locale){

        session()->put('locale', $locale);
        
        return redirect()->back();

    }
}
