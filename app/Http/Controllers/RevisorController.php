<?php

namespace App\Http\Controllers;

use App\Listing;
use Illuminate\Http\Request;

class RevisorController extends Controller
{
    public function __construct(){

        $this->middleware('auth.revisor');
    }

    public function homeRevisor(){

        $listings = Listing::where('is_accepted', null)
        ->orderBy('created_at', 'desc')
        ->get();

        return view('revisor.home', compact('listings'));
    }


    public function setAccepted($listing_id, $value){

        $listing = Listing::find($listing_id);
        $listing->is_accepted = $value;
        $listing->save();

        return redirect(route('revisor.home'));
    }

    public function accept($listing_id){

        return $this->setAccepted($listing_id, true);
    }

    public function reject($listing_id){

        return $this->setAccepted($listing_id, false);
    }

    public function delete($id){
        
        Listing::destroy($id);
        

        return redirect(route('listing.delete'));
    }

    public function archive(){

        $listings = Listing::where('is_accepted', 0)
        ->orderBy('created_at', 'desc')
        ->get();

        return view('revisor.archive', compact('listings'));

    }

    public function setReturn($listing_id, $value){

        $listing = Listing::find($listing_id);
        $listing->is_accepted = $value;
        $listing->save();

        return redirect(route('revisor.archive'));
    }
    
    public function return($listing_id){

        return $this->setReturn($listing_id, null);
    }


}
