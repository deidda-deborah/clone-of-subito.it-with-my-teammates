<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class MakeUserRevisor extends Command
{
    
    protected $signature = 'presto:makeUserRevisor';

    
    protected $description = "Rendi revisore l'utente";

    
    public function __construct()
    {
        parent::__construct();
    }

    
    public function handle()
    {
        $email = $this->ask("Inserisci la mail dell'utente che vuoi rendere revisore:");
        $user = User::where('email', $email)->first();

        if(!$user){

            $this->error('Utente non trovato!');
            return;
        }

        $user->is_revisor = true;
        $user->save();
        $this->info("L'utente {$user->name} è ora revisore.");
    }
}
