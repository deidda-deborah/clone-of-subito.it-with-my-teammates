<?php

namespace App;

use App\Listing;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class ListingImage extends Model
{
    protected $casts = [
        'labels'=>'array',
    ];
    



    public function listing(){

        return $this->belongsTo(Listing::class);
    }

    static public function getUrlByFilePath($filePath, $w = null, $h = null){

        if(!$w && !$h){

            return Storage::url($filePath);
        }

        $path = dirname($filePath);
        $fileName = basename($filePath);
        
        $file = "{$path}/crop{$w}x{$h}_{$fileName}";

        return Storage::url($file);

    }

    public function getUrl($w = null, $h = null){

        return ListingImage::getUrlByFilePath($this->file, $w, $h);
    }
}
