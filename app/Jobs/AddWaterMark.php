<?php

namespace App\Jobs;

use App\ListingImage;





use Image;
use Illuminate\Bus\Queueable;
use Intervention\Image\Point;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class AddWaterMark implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $path; 

    public function __construct($path)
    {
        $this->path = $path;
    }

    public function handle()
    {
        $imagePath = public_path($this->path);
        
        list($width, $height) = getimagesize($imagePath);

        if(!file_exists($imagePath))
            return false;

        //Prendo la grandezza dell'imaggine
        //Calcolo 1/6 dell'immagine
        //rendo il watermark l'1/5
        //inserisco il WM nell'immagine
        $image = Image::make($imagePath);
        
        $watermark = Image::make(public_path('/images/presto_out.png'));
        $watermark->resize($height/5, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $watermark->save(public_path('/images/logopresto_res.png'));

        $image->insert(public_path('/images/logopresto_res.png'));
        $image->save($imagePath);      
    }

}
