<?php

namespace App;

use App\User;
use App\Category;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Listing extends Model
{
    use Searchable;
 
    protected $fillable = ['title', 'description', 'price'];

    public function toSearchableArray()
    {
        $array =[
            'id'=> $this->id,
            'title'=> $this->title,
            'description'=> $this->description,
            'category'=> $this->category,
        ];

        return $array;
    }

    public function category(){

        return $this->belongsTo(Category::class);
    }

    public function user(){

        return $this->belongsTo(User::class);
    }

    static public function ToBeRevisionedCount(){

        return Listing::where('is_accepted', null)->count();
    }

    static public function ToBeArchiveCount(){

        return Listing::where('is_accepted', 0)->count();
    }

    public function images(){

        return $this->hasMany(ListingImage::class);
    }
}

    

